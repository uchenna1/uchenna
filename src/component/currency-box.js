import React from 'react'
import styled from 'styled-components'
import Select from 'react-select';
import { Line } from 'react-chartjs-2';

const BoxWrap = styled.div`
    width: 35%;
    box-sizing:border-box;
    // height:200px;
    padding:10px;
    background:${props => props.background};
    margin:6px;
    display:flex;
    flex-direction:column;
    box-sizing:border-box;
`
const Col1 = styled.div`
    display:flex;
    flex-direction:row;
    justify-content: space-between;
    padding: 0px 10px; 
    align-items: center;
`
const leftItem = styled.div`
   font-size: 14px;
   align-items:center;  
`
const Col2 = styled.div`
    display:flex;
    flex-direction:row;
    justify-content: space-between;
    align-items:center;
    margin-top:10px;
    padding:0px 10px;
`
const Col3 = styled.div``
const LeftContent = styled.div`
  color: #fff;
  font-size:28px;
`
const RightContent = styled.div`
    display: flex;
    flex-direction:column;
    align-items: center;
`
const HeaderTitle = styled.div`
  font-size: 10px;
  color:#fff;
`
const NumberCount = styled.div`
 font-size: 13px;
 color: #fff;
`
const FromWrap = styled.form`
    display: flex;
`

const CurrencyBox = ({ background, options, data, value, onChange}) => {
    return(
       <BoxWrap background={background}>
           <Col1>
               <form>
                   <Select
                       className="react-select"
                       classNamePrefix="react-select"
                       onChange={onChange}
                       value={value}
                    //    placeholder={placeholder}
                       options={options}
                   />
               </form>
               <leftItem><span style={{color: '#fff'}}>+5.10%</span></leftItem>
           </Col1>
           <Col2>
              <LeftContent>0.<span style={{fontSize: '18px'}}>98</span>55</LeftContent>
               <RightContent>
                   <HeaderTitle>No of Tables</HeaderTitle>
                   <NumberCount>- <span style={{textDecoration: 'underline'}}>08</span> +</NumberCount>
               </RightContent>
           </Col2>
           <Col3>
              {/* <LineChart data={chartData} options={chartOptions} width="600" height="250"/> */}
                <Line data={data} />
           </Col3>
       </BoxWrap>
    )
}

export default CurrencyBox;

