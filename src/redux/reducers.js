import {ALL_DATA} from "./Type";

export default (state, action) => {
  switch (action.type) {
    case ALL_DATA:
      return { ...state, countryData: action.payload }
    default:
      return { ...state }
  }
}