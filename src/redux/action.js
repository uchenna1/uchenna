import axios from 'axios';
import {ALL_DATA_API} from './api'
import {ALL_DATA} from './Type'

export const getAllData = () => {
    return dispatch => {
        return axios.get(`${ALL_DATA_API}`)
            .then (res => {
                console.log(res)
                let data = []
                res.data.map((item, index) => {
                    data = [...data, { label: item.currency_name, value: item.currency_name, key: index }]
                })
                dispatch({
                    type: ALL_DATA,
                    payload: data
                })
            }).catch(error => {
                console.log(error.response)
            })
    }
}

