import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

/** Import the reducer */
import reducers from './reducers';

/** Connect our store to the reducers */

export default function configStore () {
    return createStore(reducers, applyMiddleware(thunk))
}