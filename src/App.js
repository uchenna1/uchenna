import React,{Component} from 'react';
import logo from './logo.svg';
import { connect } from 'react-redux'
import './App.css';
import CurrencyBox from './component/currency-box.js'
import {getAllData} from './redux/action'
import { checkPropTypes } from 'prop-types';
// import SockJS from 'sockjs-client'

class App extends Component {
    constructor(props) {
        super(props);
        const sock = new WebSocket('wss://stocksimulator.intuhire.com');
            sock.onopen = function() {
                console.log('open');
                sock.send('uchenna');
            };
        // sock.onopen = function (event) {
        //     sock.send("Here's some text that the server is urgently awaiting!");
        // };

            sock.onmessage = function(e) {
                console.log('message', e.data);
                sock.close();
            };

            sock.onclose = function() {
                console.log('close');
            };

            this.state = {
                actions: sock,
                edited: {},
                data: [],
                currencyPair: '',
            }
    }

    componentDidMount() {
        this.props.onGetData();
    }

    handleFormSubmit = (type, selectedOption) => {
        const {edited} = this.state
        this.setState({
            [type]: selectedOption,
            edited: { ...this.state.edited, [type]: selectedOption.value }
        })
        // this.state.actions.send(edited)
    }

    render() {
        const { getData, currencyPair } = this.props
        const data = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    // label: 'My First dataset',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [65, 59, 80, 81, 56, 55, 40]
                }
            ]
        };
       return (
           <div className="App">
               <div className="Position_Center">
                   <CurrencyBox 
                        background="#711d12" 
                        options={getData && getData}
                        data={data}
                        value={currencyPair}
                        onChange={(selected) => this.handleFormSubmit('currencyPair', selected)}
                        />
                   <CurrencyBox 
                        background="#ab301f" 
                        options={getData && getData}
                        data={data}
                        value={currencyPair}
                        onChange={(selected) => this.handleFormSubmit('currencyPair', selected)}/>
                   <CurrencyBox 
                        background="#54a53a" 
                        options={getData && getData}
                        data={data}
                        value={currencyPair}
                        onChange={(selected) => this.handleFormSubmit('currencyPair', selected)}/>
                   <CurrencyBox 
                        background="#4a100c" 
                        options={getData && getData}
                        data={data}
                        value={currencyPair}
                       onChange={(selected) => this.handleFormSubmit('currencyPair', selected)}/>
               </div>
           </div>
       );
   }
}

const mapStateToProps = state => {
    return {
        getData: state.countryData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onGetData: () => dispatch(getAllData())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
